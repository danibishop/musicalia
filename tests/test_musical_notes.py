from unittest import TestCase
import musicalia.musical_notes as sut


class NotesTests(TestCase):
    def test_get_note_position(self):

        self.assertEqual(0, sut.get_note_position("C"))
        self.assertEqual(3, sut.get_note_position("Eb"))
        self.assertEqual(9, sut.get_note_position("A"))
        self.assertIsNone(sut.get_note_position("goku"))

    def test_get_note_and_scale(self):
        self.assertEqual(("C#", 5), sut.get_note_and_scale("C#5"))
        self.assertEqual(("A", 4), sut.get_note_and_scale("A4"))
        self.assertEqual(("Bb", 1), sut.get_note_and_scale("Bb1"))

    def test_get_note_delta(self):
        self.assertEqual(0, sut.get_note_delta("A4", "A4"))
        self.assertEqual(-9, sut.get_note_delta("A4", "C4"))
        self.assertEqual(-21, sut.get_note_delta("A3", "C2"))
        self.assertEqual(3, sut.get_note_delta("A4", "C5"))

    def test_get_frequency(self):
        self.assertEqual(440, sut.get_frequency("A4"))
        self.assertEqual(220, sut.get_frequency("A3"))
        self.assertEqual(880, sut.get_frequency("A5"))
        self.assertEqual(523.25, sut.get_frequency("C5"))
        self.assertEqual(103.83, sut.get_frequency("G#2"))
        self.assertEqual(7902.13, sut.get_frequency("B8"))
        self.assertEqual(16.35, sut.get_frequency("C0"))

    def test_get_nearest_note(self):
        self.assertEqual("A4", sut.get_nearest_note(440))
        self.assertEqual("A4", sut.get_nearest_note(445))

        # A semitone has a width of 1/12 so from central frequency to the edge it must be 1/24
        edge_freq = 440 * pow(2, 1.0 / 24.0)
        self.assertEqual("A#4", sut.get_nearest_note(edge_freq))
        self.assertEqual("A4", sut.get_nearest_note(edge_freq - 0.00000001))

    def test_add_semitones(self):
        self.assertEqual("B4", sut.add_semitones("A4", 2))
        self.assertEqual("C5", sut.add_semitones("A4", 3))
        self.assertEqual("A4", sut.add_semitones("A4", 0))
        self.assertEqual("A#3", sut.add_semitones("D4", -4))

    def test_get_note_from_semitones(self):
        self.assertEqual("C", sut.get_note_from_semitones(0))
        self.assertEqual("C", sut.get_note_from_semitones(0.499999))
        self.assertEqual("C#", sut.get_note_from_semitones(0.500001))
        self.assertEqual("B", sut.get_note_from_semitones(-0.51))
        self.assertEqual("D#", sut.get_note_from_semitones(3))
        self.assertEqual("B", sut.get_note_from_semitones(11))
