# Musicalia

Musicalia is a set of tools and functions to work with musical notation and sound.

It will probably be a WIP forever and ever.

## Usage

For now, it is only available through source code, so read the Development section

## Development

1. Install Python 3.x
1. Install virtualenv if you do not have it yet
1. From root directory
  1. ```> virtualenv env```
  1. ```> env\Scripts\activate.bat``` (if in Windows; for Linux use ```> ./env/Scripts/activate```)
  1. ```> pip install -r requirements txt```
1. Open the folder with your favourite Python IDE (I use PyCharm community edition)

## Contribute

Pull Requests are open :D
