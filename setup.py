#!/usr/bin/env python

from setuptools import setup

setup(
    name='Musicalia',
    version='0.0.1',
    description='Music notation and sound synthesis',
    author='Dani Ramirez',
    author_email='danibishop@protonmail.com',
    url='https://gitlab.com/danibishop/musicalia',
    packages=['musicalia.musical_notes'],
    test_suite='tests'

)
