import sounddevice as sd
import numpy
from math import log2, floor

INTERVALS_FROM_C = {

    "C": 0,
    "C#": 1, "Db": 1,
    "D": 2,
    "D#": 3, "Eb": 3,
    "E": 4,
    "F": 5,
    "F#": 6, "Gb": 6,
    "G": 7,
    "G#": 8, "Ab": 8,
    "A": 9,
    "A#": 10, "Bb": 10,
    "B": 11,
}


def get_note_position(note):
    """
    :param note:
    :return:
    """

    try:
        return INTERVALS_FROM_C[note]
    except KeyError:
        return None


def get_note_and_scale(note_name):
    return note_name[:-1], int(note_name[-1])


def get_note_delta(note_name_a, note_name_b):
    if note_name_a == note_name_b:
        return 0

    note_a, scale_a = get_note_and_scale(note_name_a)
    note_b, scale_b = get_note_and_scale(note_name_b)

    scale_delta = (scale_b - scale_a) * 12
    note_a_pos = get_note_position(note_a)
    note_b_pos = get_note_position(note_b)

    return scale_delta + note_b_pos - note_a_pos


def get_frequency(note_name):
    """

    :param note_name:
    :return:
    """

    base_frequency = 440.0  # A4
    delta = get_note_delta("A4", note_name)
    freq = base_frequency * pow(2, delta / 12.0)

    return round(freq * 100) / 100.0


def get_nearest_note(freq):
    base_frequency = 440.0  # A4
    delta_semitones = log2(freq / base_frequency) * 12

    return add_semitones("A4", delta_semitones)


def add_semitones(note_name, delta_semitones):
    note, scale = get_note_and_scale(note_name)
    pos = get_note_position(note)
    total_semitones = pos + delta_semitones
    scales_up = floor(total_semitones / 12.0)
    final_pos = total_semitones % 12
    final_note = get_note_from_semitones(final_pos)
    return final_note + str(int(scales_up + scale))


def get_note_from_semitones(semitones):
    """
    
    :param semitones:
    :return:
    """
    semitones = round(semitones) % 12
    return [note for note, interval in INTERVALS_FROM_C.items() if interval == semitones][0]


def pruebas():
    sd.default.device = 7
    fs = 48000
    sd.default.samplerate = fs
    note = 440
    A3 = 220
    A4 = 440
    A5 = 880
    t = numpy.linspace(0, fs, fs)  # 1 second at fs hertzs
    tone = numpy.sin(440 * 2 * numpy.pi * t)
    """
    import matplotlib.pylab as plt
    plt.plot(t, tone)
    plt.xlabel('Angle [rad]')
    plt.ylabel('sin(x)')
    plt.axis('tight')
    plt.show()
    # print (sd.query_devices())
    """
    print(sd.query_devices())

    sd.play(tone)
    status = sd.wait()
    print(status)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
